///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Kendal Oya <kendalo@hawaii.edu>
// @date   08 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
int main(){
long difference;
long day,secondsperday = 86400;
long secondsperyear = 31536000, year, year2;
long secondsperhour = 3600, hour;
long secondspermin = 60, min;

struct tm reference;
 reference.tm_sec = 0;
 reference.tm_mon = 0;
 reference.tm_mday = 21;
 reference.tm_hour = 4;
 reference.tm_min = 0;
 reference.tm_wday = 1;
 reference.tm_year = 2014-1900;
time_t referencetime = mktime(&reference);
printf("Reference time: %s\n",asctime(&reference));


while(1){
   time_t t = time(NULL);
   difference = difftime(t, referencetime);
   year = difference/secondsperyear;

   difference = difference - (year*secondsperyear);
   day = difference/secondsperday;

   difference = difference - (day*secondsperday);
   hour = difference/secondsperhour;

   difference = difference - (hour*secondsperhour);
   min = difference/secondspermin;

   year2 = difference - (min*secondspermin);
   printf("Years: %ld Days: %ld Hours: %ld Minutes: %ld Seconds: %ld\n", year, day, hour, min, year2);
   sleep(1);
}
   return 0;
}


